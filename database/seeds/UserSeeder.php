<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = array(
            array(
                'firstname'=>'Patrick',
                'lastname' =>'Oryono',
                'phone'    =>'0784275529',
                'username' =>'patrickoryono',
                'email'    =>'me@patrickoryono.co',
                'password' =>Hash::make('admin')
            ),
            array(
                'firstname'=>'Simon',
                'lastname' =>'Onen',
                'phone'    =>'0789693473',
                'username' =>'simononen',
                'email'    =>'osimon75@yahoo.com',
                'password' =>Hash::make('admin')
            ),

            array(
                'firstname'=>'Simon',
                'lastname' =>'Lukyamuzi',
                'phone'    =>'0784275529',
                'username' =>'lukyamuzi',
                'email'    =>'luksam@gmail.com',
                'password' =>Hash::make('admin')
            ),

            array(
                'firstname'=>'Henry',
                'lastname' =>'Draku',
                'phone'    =>'0784275529',
                'username' =>'draku',
                'email'    =>'draku.henry@gmail.com',
                'password' =>Hash::make('admin')
            ),

            array(
                'firstname'=>'Kaka',
                'lastname' =>'Ogwal',
                'phone'    =>'0788237581',
                'username' =>'kaka',
                'email'    =>'kaka@jjuzawo.com',
                'password' =>Hash::make('admin')
            ),
            array(
                'firstname'=>'Jackson',
                'lastname' =>'Abandu',
                'phone'    =>'078354673',
                'username' =>'abandu',
                'email'    =>'abandu@jackson.co',
                'password' =>Hash::make('admin')
            ),

            array(
                'firstname'=>'Anthony',
                'lastname' =>'Ashaba',
                'phone'    =>'0786784993',
                'username' =>'ashaba',
                'email'    =>'ashaba@anthony.co',
                'password' =>Hash::make('admin')
            ),

            array(
                'firstname'=>'Proscovia',
                'lastname' =>'Olanago',
                'phone'    =>'0784859373',
                'username' =>'olango',
                'email'    =>'olango@juzawo.com',
                'password' =>Hash::make('admin')
            ),

            array(
                'firstname'=>'Patrick',
                'lastname' =>'Okot',
                'phone'    =>'0785683426',
                'username' =>'patrickokot',
                'email'    =>'okot@patrick.com',
                'password' =>Hash::make('admin')
            ),
            array(
                'firstname'=>'Sophia',
                'lastname' =>'Amuge',
                'phone'    =>'0789785643',
                'username' =>'sophia',
                'email'    =>'amuge@jjuzzawo.com',
                'password' =>Hash::make('admin')
            ),

            array(
                'firstname'=>'Feliesta',
                'lastname' =>'Apio',
                'phone'    =>'078478323',
                'username' =>'apio',
                'email'    =>'apio@jjuzzawo.com',
                'password' =>Hash::make('admin')
            ),

            array(
                'firstname'=>'Aisha',
                'lastname' =>'Namugombe',
                'phone'    =>'0783869354',
                'username' =>'namugombe',
                'email'    =>'namugombe.aisha@gmail.com',
                'password' =>Hash::make('admin')
            ),

            array(
                'firstname'=>'Carol',
                'lastname' =>'Akot',
                'phone'    =>'0787866483',
                'username' =>'akot.carol',
                'email'    =>'akot@jjuzzawo.com',
                'password' =>Hash::make('admin')
            ),
            array(
                'firstname'=>'Arthur',
                'lastname' =>'Mugoya',
                'phone'    =>'0780975863',
                'username' =>'mugoya',
                'email'    =>'mugoya@juzawo.io',
                'password' =>Hash::make('admin')
            ),

            array(
                'firstname'=>'Arnold',
                'lastname' =>'Babalanda',
                'phone'    =>'0701235665',
                'username' =>'baba',
                'email'    =>'babalanda.arnold@gmail.com',
                'password' =>Hash::make('admin')
            ),

            array(
                'firstname'=>'Abdul',
                'lastname' =>'Ssebagala',
                'phone'    =>'0772657834',
                'username' =>'ssebaga',
                'email'    =>'ssebaga@mtn.co.ug',
                'password' =>Hash::make('admin')
            ),

            array(
                'firstname'=>'John Mark',
                'lastname' =>'Ssebunya',
                'phone'    =>'0774673426',
                'username' =>'ssebunya',
                'email'    =>'ssebunj@mtn.co.ug',
                'password' =>Hash::make('admin')
            ),
            array(
                'firstname'=>'Allan',
                'lastname' =>'Epic',
                'phone'    =>'0773783948',
                'username' =>'epic',
                'email'    =>'epicallan@jjuzzawo.com',
                'password' =>Hash::make('admin')
            ),

            array(
                'firstname'=>'Anita',
                'lastname' =>'Ssenja',
                'phone'    =>'078656736',
                'username' =>'senjala',
                'email'    =>'ssenjala',
                'password' =>Hash::make('admin')
            ),

            array(
                'firstname'=>'Sandra',
                'lastname' =>'Mukoda',
                'phone'    =>'0785647384',
                'username' =>'mukodas',
                'email'    =>'mokodas@mtn.co.ug',
                'password' =>Hash::make('admin')
            ),

            array(
                'firstname'=>'Brenda',
                'lastname' =>'Koburamuzi',
                'phone'    =>'078475496',
                'username' =>'koburam',
                'email'    =>'koburam@mtn.co.ug',
                'password' =>Hash::make('admin')
            ),
            array(
                'firstname'=>'Caesar',
                'lastname' =>'Odoch',
                'phone'    =>'078978645',
                'username' =>'codoch',
                'email'    =>'codoch@jjuzzawo.com',
                'password' =>Hash::make('admin')
            ),

            array(
                'firstname'=>'Derrick',
                'lastname' =>'Laber',
                'phone'    =>'0784342648',
                'username' =>'laber',
                'email'    =>'laber@domain.co',
                'password' =>Hash::make('admin')
            ),

            array(
                'firstname'=>'Charles',
                'lastname' =>'Tuhairwe',
                'phone'    =>'0785734524',
                'username' =>'tuhairc',
                'email'    =>'tuhairc@mtn.co.ug',
                'password' =>Hash::make('admin')
            )



        );

        foreach($users as $user){
            \App\User::create($user);
        }


    }
}
