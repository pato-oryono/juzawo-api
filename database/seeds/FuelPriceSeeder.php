<?php

use Illuminate\Database\Seeder;

class FuelPriceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fuel_prices = array(
            array(
                'fuel_id' => 1,
                'station_id' => 1,
                'price'  => 3500
            ),

            array(
                'fuel_id' => 2,
                'station_id' => 1,
                'price'  => 3300
            ),

            array(
                'fuel_id' => 3,
                'station_id' => 1,
                'price'  => 2800
            ),

            array(
                'fuel_id' => 4,
                'station_id' => 1,
                'price'  => 57000
            ),

            array(
                'fuel_id' => 1,
                'station_id' => 2,
                'price'  => 3500
            ),

            array(
                'fuel_id' => 2,
                'station_id' => 2,
                'price'  => 3500
            ),

            array(
                'fuel_id' => 3,
                'station_id' => 2,
                'price'  => 3500
            ),

            array(
                'fuel_id' => 4,
                'station_id' => 2,
                'price'  => 50000
            ),

            array(
                'fuel_id' => 1,
                'station_id' => 3,
                'price'  => 3500
            ),

            array(
                'fuel_id' => 2,
                'station_id' => 3,
                'price'  => 3500
            ),

            array(
                'fuel_id' => 3,
                'station_id' => 3,
                'price'  => 3500
            ),

            array(
                'fuel_id' => 4,
                'station_id' => 3,
                'price'  => 57000
            ),

            array(
                'fuel_id' => 1,
                'station_id' => 4,
                'price'  => 3500
            ),

            array(
                'fuel_id' => 2,
                'station_id' => 4,
                'price'  => 3500
            ),

            array(
                'fuel_id' => 3,
                'station_id' => 4,
                'price'  => 3500
            ),

            array(
                'fuel_id' => 4,
                'station_id' => 4,
                'price'  => 57000
            ),

            array(
                'fuel_id' => 1,
                'station_id' => 5,
                'price'  => 3800
            ),

            array(
                'fuel_id' => 2,
                'station_id' => 5,
                'price'  => 2890
            ),

            array(
                'fuel_id' => 3,
                'station_id' => 5,
                'price'  => 2500
            ),

            array(
                'fuel_id' => 4,
                'station_id' => 5,
                'price'  => 60000
            ),

            array(
                'fuel_id' => 1,
                'station_id' => 6,
                'price'  => 3200
            ),

            array(
                'fuel_id' => 2,
                'station_id' => 6,
                'price'  => 3100
            ),

            array(
                'fuel_id' => 3,
                'station_id' => 6,
                'price'  => 2800
            ),

            array(
                'fuel_id' => 4,
                'station_id' => 6,
                'price'  => 57000
            ),

            array(
                'fuel_id' => 1,
                'station_id' => 7,
                'price'  => 3600
            ),

            array(
                'fuel_id' => 2,
                'station_id' => 7,
                'price'  => 3200
            ),

            array(
                'fuel_id' => 3,
                'station_id' => 7,
                'price'  => 2800
            ),

            array(
                'fuel_id' => 4,
                'station_id' => 7,
                'price'  => 56000
            ),

            array(
                'fuel_id' => 1,
                'station_id' => 8,
                'price'  => 3350
            ),

            array(
                'fuel_id' => 2,
                'station_id' => 8,
                'price'  => 2990
            ),

            array(
                'fuel_id' => 1,
                'station_id' => 9,
                'price'  => 3567
            ),

            array(
                'fuel_id' => 2,
                'station_id' => 9,
                'price'  => 3327
            ),

            array(
                'fuel_id' => 1,
                'station_id' => 10,
                'price'  => 3456
            ),

            array(
                'fuel_id' => 2,
                'station_id' => 10,
                'price'  => 2768
            ),

            array(
                'fuel_id' => 1,
                'station_id' => 11,
                'price'  => 3213
            ),

            array(
                'fuel_id' => 2,
                'station_id' => 11,
                'price'  => 3524
            ),

            array(
                'fuel_id' => 1,
                'station_id' => 12,
                'price'  => 3243
            ),

            array(
                'fuel_id' => 2,
                'station_id' => 12,
                'price'  => 3500
            ),

            array(
                'fuel_id' => 1,
                'station_id' => 13,
                'price'  => 3241
            ),

            array(
                'fuel_id' => 2,
                'station_id' => 13,
                'price'  => 3250
            ),

            array(
                'fuel_id' => 1,
                'station_id' => 14,
                'price'  => 3450
            ),

            array(
                'fuel_id' => 2,
                'station_id' => 14,
                'price'  => 3120
            ),

            array(
                'fuel_id' => 1,
                'station_id' => 15,
                'price'  => 3560
            ),

            array(
                'fuel_id' => 2,
                'station_id' => 15,
                'price'  => 3279
            ),

            array(
                'fuel_id' => 1,
                'station_id' => 16,
                'price'  => 3790
            ),

            array(
                'fuel_id' => 2,
                'station_id' => 16,
                'price'  => 3240
            ),

            array(
                'fuel_id' => 1,
                'station_id' => 17,
                'price'  => 3256
            ),

            array(
                'fuel_id' => 3,
                'station_id' => 17,
                'price'  => 2800
            ),

            array(
                'fuel_id' => 1,
                'station_id' => 18,
                'price'  => 3672
            ),

            array(
                'fuel_id' => 2,
                'station_id' =>18 ,
                'price'  => 2689
            ),

            array(
                'fuel_id' => 1,
                'station_id' => 19,
                'price'  => 3456
            ),

            array(
                'fuel_id' => 2,
                'station_id' => 19,
                'price'  => 3248
            ),

            array(
                'fuel_id' => 1,
                'station_id' => 20,
                'price'  => 3789
            ),

            array(
                'fuel_id' => 2,
                'station_id' => 20,
                'price'  => 3200
            ),

            array(
                'fuel_id' => 1,
                'station_id' => 21,
                'price'  => 3578
            ),

            array(
                'fuel_id' => 2,
                'station_id' => 22,
                'price'  => 3278
            ),

            array(
                'fuel_id' => 1,
                'station_id' => 23,
                'price'  => 3214
            ),

            array(
                'fuel_id' => 2,
                'station_id' => 23,
                'price'  => 3123
            ),

            array(
                'fuel_id' => 1,
                'station_id' => 24,
                'price'  => 3678
            ),

            array(
                'fuel_id' => 2,
                'station_id' => 24,
                'price'  => 3278
            ),
            array(
                'fuel_id' => 3,
                'station_id' => 24,
                'price'  => 2800
            ),

            array(
                'fuel_id' => 1,
                'station_id' => 25,
                'price'  => 3278
            ),
            array(
                'fuel_id' => 2,
                'station_id' => 24,
                'price'  => 3250
            ),

            array(
                'fuel_id' => 2,
                'station_id' => 25,
                'price'  => 3300
            ),
            array(
                'fuel_id' => 1,
                'station_id' => 25,
                'price'  => 3600
            ),

            array(
                'fuel_id' => 3,
                'station_id' => 25,
                'price'  => 2750
            ),

            array(
                'fuel_id' => 4,
                'station_id' => 25,
                'price'  => 56000
            ),

            array(
                'fuel_id' => 1,
                'station_id' => 26,
                'price'  => 3800
            ),
            array(
                'fuel_id' => 2,
                'station_id' => 26,
                'price'  => 3200
            ),

            array(
                'fuel_id' => 3,
                'station_id' => 26,
                'price'  => 2800
            ),

            array(
                'fuel_id' => 4,
                'station_id' => 26,
                'price'  => 60000
            ),

            array(
                'fuel_id' => 1,
                'station_id' => 27,
                'price'  => 3678
            ),

            array(
                'fuel_id' => 2,
                'station_id' => 27,
                'price'  => 3240
            ),

            array(
                'fuel_id' => 3,
                'station_id' => 27,
                'price'  => 56000
            ),
            array(
                'fuel_id' => 1,
                'station_id' => 28,
                'price'  => 3678
            ),

            array(
                'fuel_id' => 2,
                'station_id' => 28,
                'price'  => 3278
            ),

            array(
                'fuel_id' => 3,
                'station_id' => 28,
                'price'  => 2600
            ),

            array(
                'fuel_id' => 1,
                'station_id' => 29,
                'price'  => 3778
            ),

            array(
                'fuel_id' => 2,
                'station_id' => 29,
                'price'  => 3678
            ),

            array(
                'fuel_id' => 3,
                'station_id' => 29,
                'price'  => 2600
            ),

            array(
                'fuel_id' => 1,
                'station_id' => 30,
                'price'  => 3678
            ),

            array(
                'fuel_id' => 2,
                'station_id' => 30,
                'price'  => 3278
            ),

            array(
                'fuel_id' => 4,
                'station_id' => 30,
                'price'  => 56000
            ),

            array(
                'fuel_id' => 1,
                'station_id' => 31,
                'price'  => 3278
            ),

            array(
                'fuel_id' => 2,
                'station_id' => 31,
                'price'  => 3120
            ),

            array(
                'fuel_id' => 1,
                'station_id' => 32,
                'price'  => 3678
            ),

            array(
                'fuel_id' => 2,
                'station_id' => 32,
                'price'  => 3200
            ),

            array(
                'fuel_id' => 1,
                'station_id' => 33,
                'price'  => 3678
            ),

            array(
                'fuel_id' => 2,
                'station_id' => 33,
                'price'  => 3278
            ),

            array(
                'fuel_id' => 1,
                'station_id' => 34,
                'price'  => 3678
            ),

            array(
                'fuel_id' => 2,
                'station_id' => 34,
                'price'  => 3245
            ),

            array(
                'fuel_id' => 1,
                'station_id' => 35,
                'price'  => 3778
            ),

            array(
                'fuel_id' => 2,
                'station_id' => 35 ,
                'price'  => 3278
            ),

            array(
                'fuel_id' => 1,
                'station_id' => 36,
                'price'  => 3678
            ),

            array(
                'fuel_id' => 2,
                'station_id' => 36,
                'price'  => 3278
            ),

            array(
                'fuel_id' => 3,
                'station_id' => 36,
                'price'  => 2800
            ),

            array(
                'fuel_id' => 1,
                'station_id' => 37,
                'price'  => 3678
            ),

            array(
                'fuel_id' => 2,
                'station_id' => 37,
                'price'  => 3278
            ),

            array(
                'fuel_id' => 1,
                'station_id' => 38,
                'price'  => 3678
            ),

            array(
                'fuel_id' => 2,
                'station_id' => 38,
                'price'  => 3278
            ),

            array(
                'fuel_id' => 1,
                'station_id' => 39,
                'price'  => 3678
            ),

            array(
                'fuel_id' => 2,
                'station_id' => 40,
                'price'  => 3278
            ),

            array(
                'fuel_id' => 1,
                'station_id' => 41,
                'price'  => 3678
            ),

            array(
                'fuel_id' => 2,
                'station_id' => 42,
                'price'  => 3278
            ),

            array(
                'fuel_id' => 1,
                'station_id' => 42,
                'price'  => 3678
            ),

            array(
                'fuel_id' => 3,
                'station_id' => 42,
                'price'  => 2700
            ),

            array(
                'fuel_id' => 1,
                'station_id' => 43,
                'price'  => 3678
            ),

            array(
                'fuel_id' => 2,
                'station_id' => 43,
                'price'  => 3278
            ),

            array(
                'fuel_id' => 1,
                'station_id' => 44,
                'price'  => 3678
            ),

            array(
                'fuel_id' => 2,
                'station_id' => 44,
                'price'  => 3278
            ),

            array(
                'fuel_id' => 3,
                'station_id' => 44,
                'price'  => 2750
            ),

            array(
                'fuel_id' => 1,
                'station_id' => 40,
                'price'  => 3900
            ),

            array(
                'fuel_id' => 3,
                'station_id' => 40,
                'price'  => 2650
            ),

            array(
                'fuel_id' => 4,
                'station_id' => 40,
                'price'  => 50000
            ),



        );

        foreach($fuel_prices as $fuel_price){
            \App\FuelPrice::create($fuel_price);
        }
    }
}
