<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(FuelTypeSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(FuelPriceSeeder::class);
        $this->call(StationSeeder::class);
    }
}
