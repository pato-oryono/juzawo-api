<?php

use Illuminate\Database\Seeder;

class StationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $stations = array(
            array(
                'brand_name'=>'Olympus',
                'location' =>'Rubaga Rd, Kampala',
                'lat'    =>'0.3644283',
                'lng' =>'32.5445848',
                'user_id'    => 1,
                'logo' => 'logos/don.jpg'
            ),

            array(
                'brand_name'=>'Mogas',
                'location' =>'Kampala Road, Entebbe',
                'lat'    =>'0.0750421',
                'lng' =>'32.4660925',
                'user_id'    => 2,
                'logo' => 'logos/mogas.png'
            ),

            array(
                'brand_name'=>'Kobil',
                'location' =>'Kampala - Entebbe Rd',
                'lat'    =>'0.0750421',
                'lng' =>'32.4660925',
                'user_id'    => 3,
                'logo' => 'logos/don.jpg'
            ),

            array(
                'brand_name'=>'Gapco',
                'location' =>'Kiwafu, Entebbe',
                'lat'    =>'0.0750421',
                'lng' =>'32.4660925',
                'user_id'    => 4,
                'logo' => 'logos/gapco.png'
            ),

            array(
                'brand_name'=>'Gaz',
                'location' =>'Kampala - Entebbe Rd, Kajjansi',
                'lat'    =>'0.2079029',
                'lng' =>'32.47094',
                'user_id'    => 5,
                'logo' => 'logos/gaz.png'
            ),

            array(
                'brand_name'=>'Hared',
                'location' =>'Kampala',
                'lat'    =>'0.1633751',
                'lng' =>'32.4835967',
                'user_id'    => 6,
                'logo' => 'logos/don.jpg'
            ),


            array(
                'brand_name'=>'Moil',
                'location' =>'Kampala - Entebbe Rd, Kisubi',
                'lat'    =>'0.1551655',
                'lng' =>'32.4650474',
                'user_id'    => 7,
                'logo' => 'logos/don.jpg'
            ),

            array(
                'brand_name'=>'Tan Oil',
                'location' =>'Entebe',
                'lat'    =>'0.06063',
                'lng' =>'32.4137306',
                'user_id'    => 8,
                'logo' => 'logos/don.jpg'
            ),

            array(
                'brand_name'=>'Shell',
                'location' =>'Entebe',
                'lat'    =>'0.0554788',
                'lng' =>'32.4031261',
                'user_id'    => 9,
                'logo' => 'logos/shell.png'
            ),

            array(
                'brand_name'=>'Shell',
                'location' =>'Airport Road, Entebbe',
                'lat'    =>'0.0562933',
                'lng' =>'32.4066991',
                'user_id'    => 10,
                'logo' => 'logos/shell.png'
            ),

            array(
                'brand_name'=>'Total',
                'location' =>'Obote Avenue, Lira',
                'lat'    =>'2.123717',
                'lng' =>'32.5784693',
                'user_id'    => 11,
                'logo' => 'logos/total.png'
            ),

            array(
                'brand_name'=>'Shell',
                'location' =>'Lira',
                'lat'    =>'2.1243905',
                'lng' =>'32.577782',
                'user_id'    => 12,
                'logo' => 'logos/shell.png'
            ),

            array(
                'brand_name'=>'Gapco',
                'location' =>'Lira - Gulu Rd',
                'lat'    =>'2.1738788',
                'lng' =>'32.742451',
                'user_id'    => 13,
                'logo' => 'logos/gapco.png'
            ),

            array(
                'brand_name'=>'Petrol',
                'location' =>'Lira',
                'lat'    =>'2.1250641',
                'lng' =>'32.5770947',
                'user_id'    => 14,
                'logo' => 'logos/don.jpg'
            ),


            array(
                'brand_name'=>'Petrol',
                'location' =>'Lira',
                'lat'    =>'2.1257376',
                'lng' =>'32.5764074',
                'user_id'    => 15,
                'logo' => 'logos/don.jpg'
            ),

            array(
                'brand_name'=>'Shell',
                'location' =>'Lira',
                'lat'    =>'2.1264111',
                'lng' =>'32.5757201',
                'user_id'    => 16,
                'logo' => 'logos/shell.png'
            ),

            array(
                'brand_name'=>'Gapco',
                'location' =>'Lira - Gulu Rd, Lira',
                'lat'    =>'2.1270846',
                'lng' =>'32.5750328',
                'user_id'    => 17,
                'logo' => 'logos/gapco.png'
            ),


            array(
                'brand_name'=>'Golden gas',
                'location' =>'Juba Road, Lira',
                'lat'    =>'2.1257001',
                'lng' =>'32.5791549',
                'user_id'    => 18,
                'logo' => 'logos/don.jpg'
            ),


            array(
                'brand_name'=>'Rhino oils',
                'location' =>'Soroti Road, Lira',
                'lat'    =>'2.1284317',
                'lng' =>'32.5736582',
                'user_id'    => 19,
                'logo' => 'logos/don.jpg'
            ),


            array(
                'brand_name'=>'Shell',
                'location' =>'Lira - Gulu Rd, Lira',
                'lat'    =>'2.1291052',
                'lng' =>'32.5729709',
                'user_id'    => 20,
                'logo' => 'logos/shell.png'
            ),

            array(
                'brand_name'=>'Petrocity',
                'location' =>'Kabale - Mbarara Rd, Mbarara',
                'lat'    =>'0.591607',
                'lng' =>'30.6202272',
                'user_id'    => 21,
                'logo' => 'logos/don.jpg'
            ),

            array(
                'brand_name'=>'Total',
                'location' =>'Lake Resort Hotel Road, Mbarara',
                'lat'    =>'0.5914351',
                'lng' =>'30.6200555',
                'user_id'    => 22
            ),

            array(
                'brand_name'=>'Shell',
                'location' =>'Koranorya, Mbarara',
                'lat'    =>'0.5914351',
                'lng' =>'30.6200555',
                'user_id'    => 23,
                'logo' => 'logos/shell.png'
            ),

            array(
                'brand_name'=>'Kobil',
                'location' =>'Kampala Road, Mbarara',
                'lat'    =>'0.5912632',
                'lng' =>'30.6198838',
                'user_id'    => 24,
                'logo' => 'logos/don.jpg'
            ),

            array(
                'brand_name'=>'Total',
                'location' =>'Kampala Road, Mbarara',
                'lat'    =>'0.5910913',
                'lng' =>'30.6197121',
                'user_id'    => 10,
                'logo' => 'logos/total.png'
            ),

            array(
                'brand_name'=>'Hared',
                'location' =>'Soroti - Mbale Rd, Mbale',
                'lat'    =>'1.0967538',
                'lng' =>'34.1498796',
                'user_id'    => 12,
                'logo' => 'logos/don.jpg'
            ),

            array(
                'brand_name'=>'Total',
                'location' =>'Mbale',
                'lat'    =>'1.0970967',
                'lng' =>'34.1495362',
                'user_id'    => 13,
                'logo' => 'logos/total.png'
            ),

            array(
                'brand_name'=>'Total',
                'location' =>'Mbale',
                'lat'    =>'1.0742503',
                'lng' =>'34.1526264',
                'user_id'    => 15,
                'logo' => 'logos/total.png'
            ),

            array(
                'brand_name'=>'Agrip',
                'location' =>'Mbale',
                'lat'    =>'1.0969252',
                'lng' =>'34.1497079',
                'user_id'    => 16,
                'logo' => 'logos/don.jpg'
            ),

            array(
                'brand_name'=>'Gapco',
                'location' =>'Pallisa, Mbale',
                'lat'    =>'1.0970109',
                'lng' =>'34.1496221',
                'user_id'    => 17,
                'logo' => 'logos/gapco.png'
            ),

            array(
                'brand_name'=>'Gapco',
                'location' =>'Masindi',
                'lat'    =>'1.6177354',
                'lng' =>'31.5518409',
                'user_id'    => 18,
                'logo' => 'logos/gapco.png'
            ),

            array(
                'brand_name'=>'Kobil',
                'location' =>'Masindi',
                'lat'    =>'1.6184121',
                'lng' =>'31.5511536',
                'user_id'    => 19,
                'logo' => 'logos/don.jpg'
            ),

            array(
                'brand_name'=>'Shell',
                'location' =>'Bobi - Masindi Rd, Masindi',
                'lat'    =>'1.6197656',
                'lng' =>'31.5497789',
                'user_id'    => 20,
                'logo' => 'logos/shell.png'
            ),

            array(
                'brand_name'=>'Fuelex',
                'location' =>'Kampala - Gulu Highway',
                'lat'    =>'1.6394727',
                'lng' =>'31.6038387',
                'user_id'    => 21,
                'logo' => 'logos/don.jpg'
            ),

            array(
                'brand_name'=>'Total',
                'location' =>'Gulu',
                'lat'    =>'2.3152418',
                'lng' =>'31.8146973',
                'user_id'    => 22,
                'logo' => 'logos/total.png'
            ),

            array(
                'brand_name'=>'Don Fuel',
                'location' =>'Bobi - Masindi Rd, Kigumba',
                'lat'    =>'1.639714',
                'lng' =>'31.6036008',
                'user_id'    => 23,
                'logo' => 'logos/don.jpg'
            ),

            array(
                'brand_name'=>'Moil',
                'location' =>'Akokoro Road, Apac',
                'lat'    =>'1.6420235',
                'lng' =>'31.1807193',
                'user_id'    => 24,
                'logo' => 'logos/don.jpg'
            ),

            array(
                'brand_name'=>'Kobil',
                'location' =>'Chegere Road, Apac',
                'lat'    =>'1.6446126',
                'lng' =>'31.1779621',
                'user_id'    => 5,
                'logo' => 'logos/don.jpg'
            ),

            array(
                'brand_name'=>'Gapco',
                'location' =>'Lira - Gulu Rd, Lira',
                'lat'    =>'1.6472015',
                'lng' =>'31.1752048',
                'user_id'    => 6,
                'logo' => 'logos/gapco.png'
            ),

            array(
                'brand_name'=>'Shell',
                'location' =>'Gulu',
                'lat'    =>'2.3127195',
                'lng' =>'31.8174525',
                'user_id'    => 7,
                'logo' => 'logos/shell.png'
            ),

            array(
                'brand_name'=>'Delta',
                'location' =>'Gulu',
                'lat'    =>'2.317764',
                'lng' =>'31.811942',
                'user_id'    => 8,
                'logo' => 'logos/don.jpg'
            ),

            array(
                'brand_name'=>'Gapco',
                'location' =>'Lira - Gulu Rd, Lira',
                'lat'    =>'1.6523789',
                'lng' =>'31.1696847',
                'user_id'    => 9,
                'logo' => 'logos/gapco.png'
            ),

            array(
                'brand_name'=>' Total',
                'location' =>'Kitgum',
                'lat'    =>'3.2990843',
                'lng' =>'32.3186072',
                'user_id'    => 10,
                'logo' => 'logos/total.png'
            ),

            array(
                'brand_name'=>'Shell',
                'location' =>'Gulu-Kitgum Rd, Kitgum',
                'lat'    =>'3.3039399',
                'lng' =>'32.3130871',
                'user_id'    => 11,
                'logo' => 'logos/shell.png'
            )




        );

        foreach($stations as $station){
            \App\Station::create($station);
        }
    }
}
