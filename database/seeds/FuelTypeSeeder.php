<?php

use Illuminate\Database\Seeder;

class FuelTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $fuel_types = array(
            array(
                'name' => 'petroleum',
                'description' => 'For small engines such as generators, saloon cars'
            ),

            array(
                'name' => 'diesel',
                'description' => 'For heavy duty engines and machines such as automated electricity generators and trucks'
            ),

            array(
                'name' => 'kerosene',
                'description' => 'This is fuel for lighting and cooking using stoves'
            ),

            array(
                'name' => 'gas',
                'description' => 'For cooking'
            )

        );

        foreach($fuel_types as $fuel_type){
            \App\FuelType::create($fuel_type);
        }
    }
}
