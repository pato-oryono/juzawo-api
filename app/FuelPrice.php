<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FuelPrice extends Model
{
    protected $fillable = [
        'fuel_id', 'station_id', 'price',
    ];

    public function fuel_type()
    {
        return $this->belongsTo('\App\FuelType','fuel_id');
    }

    public function station()
    {
        return $this->belongsTo('\App\Station','station_id');
    }
}
