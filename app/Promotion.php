<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{
    protected $fillable = [
        'name',
        'description',

    ];

    public function station()
    {
        return $this->belongsTo(Station::class, 'station_id');
    }
}
