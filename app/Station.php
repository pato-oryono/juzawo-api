<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Station extends Model
{
    protected $fillable = [
        'brand_name', 'location', 'lat','lng','admin','logo'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function price()
    {
        return $this->hasMany('App\FuelPrice');
    }

    public function services()
    {
        return $this->hasMany(Service::class, 'station_id');
    }

    public function promotions()
    {
        return $this->hasMany(Promotion::class, 'station_id');
    }

}
