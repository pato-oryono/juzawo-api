<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prospect extends Model
{
    protected $fillable = [
        'firstname', 'lastname', 'email','phone'
    ];
}
