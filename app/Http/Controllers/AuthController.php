<?php

namespace App\Http\Controllers;

use HttpResponse;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\JWTAuth;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if(isset($credentials)){
            if (Auth::attempt($credentials)) {

                return response()->json([
                    'success' => 'Login Successful',
                    'user'=> \App\User::where('id',Auth::user()->id)->get(),
                    'station' => [
                        'id' => \App\User::find(Auth::user()->id)->station->id,
                        'brand' => \App\User::find(Auth::user()->id)->station->brand_name,
                        'location'=>\App\User::find(Auth::user()->id)->station->location
                    ]
                ],  200);

            }else
            {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }

        }

        return response()->json(['error' => 'fields empty'], 401);






    }

    public function signup(Request $request)
    {

        $prospect = \App\Prospect::create($request->all());

        if($prospect){
            return response()->json([
                'success' => 'Registration successful',
                'data' => $prospect
            ], 200);

        }else{
            return response()->json(['error'=>'Registration unsuccessful']);
        }


    }

}
