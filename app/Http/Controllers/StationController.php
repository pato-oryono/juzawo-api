<?php

namespace App\Http\Controllers;

use App\Station;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;

class StationController extends Controller
{
    public function index(Request $request)
    {
        if($request->has('lat') && $request->has('long')){
            $latitude = $request->get('lat');
            $longitude = $request->get('long');
            $limit = $request->get('limit');

            $query = "SELECT id, logo, brand_name, location, tags, lat, lng, user_id, lat-".$latitude." AS cLat, lng-".$longitude. " AS cLong, SQRT(POW(lat-".$latitude.",". 2 .")+POW(lng-".$longitude."," . 2 . ")) AS distance FROM stations ORDER BY distance ASC LIMIT ".$limit;
            /*echo $query;*/
            $stations = DB::select(DB::raw($query));

            /*$stations = \App\Station::all();*/

            return response()->json($stations,200);


        }
        
        return response()->json(\App\Station::with(['user', 'price.fuel_type','services'])->get(), 200);
    }

    public function show($id)
    {
        $station = \App\Station::with(['user','price.fuel_type', 'services', 'promotions'])->find($id);
        return response()->json($station);
    }

    public function update($id, Request $request)
    {
        $price = \App\FuelPrice::where('station_id', $id);
        $price->update($request->all());
        return response()->json([
            'success' => 'Update successful',
            'data' => $price

        ], 200);
    }

    public function prices(Request $request)
    {
        $station_id = $request->get('station_id');
        /*dd($station_id);*/
        $prices = \App\FuelPrice::with(['fuel_type','station'])
            ->where('station_id', $station_id)
            ->get();
        return response()->json($prices);
    }
}
