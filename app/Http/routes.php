<?php



/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use Illuminate\Support\Facades\Input;
use Tymon\JWTAuth\JWTAuth;


Route::get('/', function () {
    /*return view('welcome');*/

    return response()->json(['Jjuzzawo API live' => date("D M j G:i:s")]);
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});

Route::get('/stations','StationController@index');

Route::get('/stations/{id}','StationController@show');

Route::post('/stations/update/{id}','StationController@update');

Route::post('auth/login', 'AuthController@login');


Route::post('auth/signup', 'AuthController@signup');

/*Route::get('stations/count', 'StationController@does');*/


Route::get('/search',function(){
    $query = Input::get('q');
    if($query){
        $stations = \App\Station::where('brand_name', 'LIKE', '%' . $query . '%')
            ->orWhere('location', 'LIKE', '%' . $query . '%')
            ->orWhere('tags', 'LIKE', '%' . $query . '%')->get();
        return Response()->json($stations);
    }else return Response()->json(\App\Station::all());



});

Route::get('/users',function(){
    $users = \App\User::all();
    return response()->json($users);

});



