<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FuelType extends Model
{
    protected $fillable = [
        'name', 'description',
    ];

    public function station()
    {
        return $this->belongsToMany('\App\Station');
    }


}
